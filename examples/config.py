import os
import tempfile

basedir = os.path.abspath(os.path.dirname(__file__))

class BaseConfig():

    '''Contains the base configuration '''

    CLIENT_MODULE='Test App'

    INSTANCE_FOLDER_PATH = os.path.join(tempfile.gettempdir(), 'base_app')
    LOG_FOLDER = INSTANCE_FOLDER_PATH
    if not os.path.exists(INSTANCE_FOLDER_PATH):
        os.makedirs(INSTANCE_FOLDER_PATH)

    SECRET_KEY = 'This string will be replaced with a proper key in production.'
    if 'DATABASE_URL' in os.environ:
        SQLALCHEMY_DATABASE_URI = os.environ['DATABASE_URL'].replace("'", "")
    else:
        SQLALCHEMY_DATABASE_URI = 'sqlite:///testdata.db'

    DATABASE_CONNECT_OPTIONS = {}
    DATABASE_URL = SQLALCHEMY_DATABASE_URI

    CSRF_ENABLED = True
    CSRF_SESSION_KEY = "jumpingoversmallpigsflyingonsquerylsiscool"

    DEBUG = False
    # ADMIN = 'admin'
    # ADMIN_PASS = 'kook'
    EMAIL = 'test@test.com'


class DebugConfig(BaseConfig):

    '''Identical to BaseConfig but enables debugging '''

    DEBUG = True


class TestConfig(BaseConfig):

    '''Contains the configuration for testing '''

    TESTING = True
    WTF_CSRF_ENABLED = False
    SQLALCHEMY_ECHO = False

    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + BaseConfig.INSTANCE_FOLDER_PATH + 'testdata.db'
    DATABASE_URL = SQLALCHEMY_DATABASE_URI
