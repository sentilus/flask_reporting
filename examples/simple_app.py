__author__ = 'phillip'


from flask import Flask, redirect
from flask_reporting import Reporting
from flask_reporting import IBaseAnalysis
from flask_sentifront import Frontend
from flask_sqlalchemy import SQLAlchemy
import abc

from sqlalchemy.orm import relationship
from flask_security import UserMixin
import config
import pandas as pd
import numpy as np
from bs4 import BeautifulSoup
import wtforms
from wtforms import Form
from bs4 import Tag, NavigableString


app = Flask(__name__, static_folder='static')
app.config.from_object(config.DebugConfig)

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///testdata.db'

db = SQLAlchemy()

db.init_app(app)

report = Reporting()

frontend = Frontend()

user_role=frontend.default_user_role(db)

Role =frontend.default_role(db)

class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(255), unique=True)
    password = db.Column(db.String(255))
    active = db.Column(db.Boolean())
    confirmed_at = db.Column(db.DateTime())
    name = db.Column(db.String)

    roles = db.relationship('Role', secondary=user_role,
                            backref=db.backref('users', lazy='dynamic'))


    access_reports = relationship("SessionReports")

    @classmethod
    def get_by_username(cls, username):
        """
        :rtype : User
        """
        return cls.query.filter_by(name=username).first()


@app.route("/")
def hello():
    return redirect('/reporting/public')

frontend.init_app(app, db, user=User, role=Role, user_role= user_role)
report.init_app(app, db ,user=User)



class StartEndDate(Form):
    start_date = wtforms.fields.DateTimeField('Start-date')
    end_date = wtforms.fields.DateTimeField('End-date')


def clone(el):
    if isinstance(el, NavigableString):
        return type(el)(el)

    copy = Tag(None, el.builder, el.name, el.namespace, el.nsprefix)
    # work around bug where there is no builder set
    # https://bugs.launchpad.net/beautifulsoup/+bug/1307471
    copy.attrs = dict(el.attrs)
    for attr in ('can_be_empty_element', 'hidden'):
        setattr(copy, attr, getattr(el, attr))
    for child in el.contents:
        copy.append(clone(child))
    return copy

class SimpleReport(IBaseAnalysis):
    """ Pivot table analysis of timesheet data """

    def __init__(self):
        self._name = 'simple_report'
        self._template = 'simple_table.html'
        self._form = StartEndDate()

    def run(self, form=None):
        """

        :param form:
        :return: dictionary of parameter value pairs
        :rtype:dict()
        """
        #You can get a dataframe from data in the db
        form = self.setup_form(form)
        d = {'one' : [1., 2., 3., 4.],
         'two' : [4., 3., 2., 1.],
         'more' : ['a', 'a', 'b', 'b']}
        data = pd.DataFrame(d)

        #beautifulsoup can be used to perform additional manipulation of the table
        htmltable = BeautifulSoup(data.to_html(index=False
                                                       ))
        tabletag = htmltable.find('table')
        del tabletag['border']
        tabletag['id'] = 'example'
        tabletag['class'] = 'display controller table-hover table '
        headertag = htmltable.find('thead')
        foottag=clone(headertag)
        foottag.name = 'tfoot'
        headertag.append(foottag)
        self._form=form

        return dict(table=str(htmltable),form=form)

    def name(self):
        return self._name

    def template(self):
        return self._template

    def form(self):
        return self._form


report.add_analysis(SimpleReport)

if __name__=='__main__':

    with app.app_context():
        db.create_all()
        user_admin=User.get_by_username('admin')
        if user_admin is None:
            app.config['user_datastore'].create_user(email='admin',name='admin',
                                                         password='1234567')
            db.session.commit()

        #All reports can be viewed by common
        user_common=User.get_by_username('common')
        if user_common is None:
            app.config['user_datastore'].create_user(email='common',name='common',
                                                         password='1234567')
            db.session.commit()
        # this method needs to be executed within the app contexts
        report.populate_base_config()

    # Add administrative views here

    app.run(debug=True)

