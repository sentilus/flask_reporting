from .base import IBaseAnalysis, Reporting


def populate_analysis_reports(active_analysis, session,
                              analysis_cls, reports_cls):

    """Populates the analysis and report tables with defaults"""

    # Insert views into the database
    # Gets a list of all the analysis

    i = 1
    for member in active_analysis:
        view_class = member

        view_inst = view_class()
        name = view_class.__name__
        rv = session.query(analysis_cls).filter_by(
            classname=name).first()
        record = analysis_cls(**{'classname': name, 'name': view_inst.name()})
        if rv is None:
            session.add(record)
            session.commit()
            rv2 = session.query(analysis_cls).filter_by(
                classname=name).first()

            session.add(reports_cls(
                **{'id': i, 'name': record.name}))
            session.add(reports_cls(
                **{'user_id': None, 'id': i + 1,
                   'name': record.name + ' template', 'parent_id': i,
                   'analysis_id': rv2.id}))
            i += 2
            session.commit()