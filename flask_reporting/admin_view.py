from .views import model_view_base, public_base, save_analysis_base
from flask import (Blueprint, render_template, current_app, request, g,
                   flash, url_for, redirect, session, abort)
from flask_security import login_required,  current_user

from . import utils
from .utils import ReportingUtility
from .forms import ReportNameForm
from .base import Reporting
from flask.ext.admin import BaseView, expose
from .views import get_analysis_from_module_list

#TODO add security decorators
class ReportView(BaseView, ReportingUtility):

    def __init__(self, session, user_cls, reports_cls, analysis_cls,
                 *args, **kwargs):

        # super(ReportView, self).__init__(*args, **kwargs)
        BaseView.__init__(self, *args, **kwargs)
        ReportingUtility.__init__(self, session, user_cls,
                                  reports_cls, analysis_cls)

    @expose('/')
    def public(self):
        """Public interface shows branding and report tree"""
        # return public_base()

        G = self.get_user_views(current_user)
        roots = [n for n, d in G.in_degree().items() if d == 0]
        return self.render(
            'admin_base_report.html', G=G, roots=roots,
            branding=Reporting.branding, base_template=Reporting.base_template,
            appbuilder=Reporting.appbuilder)

    @expose('/<int:analysis_id>/<int:report_id>/save', methods=['POST'])
    def savemodel(self, analysis_id, report_id=None):
        """ Allows saving of a reports"""
        return save_analysis_base(self.session, analysis_id, report_id,
                                  self.reports_cls, self.analysis_cls)

    @expose('/<int:analysis_id>/<int:report_id>', methods=['GET', 'POST'])
    def modelview(self, analysis_id, report_id=None):
        """ The main routing for flask reporting"""
        rv = self.session.query(self.analysis_cls).filter_by(
            id=analysis_id).first()
        viewclass = get_analysis_from_module_list(Reporting.all_analyses,
                                                  rv.classname)
        if viewclass is not None:
            view_inst = viewclass()
            G = self.get_user_views(current_user)

            if hasattr(view_inst, 'form'):
                kwargs = self.get_kwargs(request, view_inst, report_id)

            else:
                kwargs = view_inst.run()
                kwargs['form'] = None
            kwargs['G'] = G
            kwargs['roots'] = [n for n, d in G.in_degree().items() if d == 0]
            kwargs['ReportNameForm'] = ReportNameForm()
            kwargs['reporting_template'] = view_inst.template()

            kwargs['branding'] = Reporting.branding
            kwargs['base_template'] = Reporting.base_template
            if Reporting.appbuilder is not None:
                kwargs['appbuilder'] = Reporting.appbuilder

            return self.render(kwargs['reporting_template'], **kwargs)
        else:
            return abort(500)

    @expose('/<int:analysis_id>/<int:report_id>/loadState', methods=['POST'])
    def loadtable(self, analysis_id, report_id=None):
        """ Allows saving of datatables state"""
        data=self.session.\
            query(SessionSavedTables).\
            filter(SessionAnalysis.id==analysis_id,\
                                      SessionReports.id==report_id).first()
        if data is None:
            return ''
        else:
            return data.json_config

    @expose('/<int:analysis_id>/<int:report_id>/saveState', methods=['POST'])
    def savetable(self, analysis_id, report_id=None):
        """ Allows saving of a reports datatables state"""

        newObj=self.session.query(SessionSavedTables).filter(SessionSavedTables.report_id==report_id).first()
        if newObj is None:
            newObj = SessionSavedTables()
        newObj.report_id = report_id
        newObj.json_config = json.dumps(request.json)
        newObj.table_id = 1
        self.session.merge(newObj)
        self.session.commit()
        return 'TableStateSaved'