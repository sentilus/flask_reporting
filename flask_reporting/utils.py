"""
Contains utilities for the common tool

"""

from .base import Reporting
import networkx as nx
import sqlalchemy
import re
import json
from bs4 import Tag, NavigableString
from flask import render_template_string, render_template

from flask_security import current_user
from sqlalchemy import or_


class ReportingUtility(object):

    def __init__(self, session, user_cls, reports_cls, analysis_cls,
                 table_cls):
        self.init_utility(session, user_cls, reports_cls, analysis_cls,
                          table_cls)

    def init_utility(self, session, user_cls, reports_cls, analysis_cls,
                     table_cls):
        self.session = session
        self.reports_cls = reports_cls
        self.user_cls = user_cls
        self.analysis_cls = analysis_cls
        self.table_cls = table_cls

    def save_current_config(self, report_id, view_inst, request):
        """ Saves the current configuration in the request in the report tree

        :param request: A Flask request object
        :param view_inst: The current view instance
        :param report_id: The report_id

        """
        currReport = self.session.query(self.reports_cls).filter(
            self.reports_cls.id == report_id).first()

        newReport = self.reports_cls()
        form_class = view_inst.form().__class__
        form2 = dict(**request.form)
        form2.pop("report_name")
        form2.pop("csrf_token")
        form2 = form_class(**form2)

        form2 = view_inst.set_form_choices(form2)

        if form2.validate():
            newReport.name = request.form['report_name']
            newReport.parent_id = currReport.parent_id
            newReport.analysis_id = currReport.analysis_id
            newReport.form = json.dumps(form2.data)
            newReport.user_id = current_user.id
            self.session.add(newReport)
            self.session.commit()
            G = self.get_user_views(current_user)
            roots = [n for n, d in G.in_degree().items() if d == 0]
            return render_template('reporting/sidebar_div.html',G=G,roots=roots)
        else:
            return 'Form contains validation errors'


    def get_kwargs(self, request, view_inst, report_id):
        """ Gets the key value arguments for the analysis template


        :param request: A Flask request object
        :param view_inst: The current view instance
        :param report_id: The report_id
        :type report_id : int
        :rtype : dict
        :return: Dictionary

        >>>get_kwargs(request, view_inst,report_id )
        """
        kwargs=dict()
        if request.method == 'POST':

            if hasattr(request, 'form'):
                kwargs = view_inst.run(form=dict(**request.form))
                if 'Save' in request.form:
                    print('TODO fix properly')
                    # save_current_config(report_id, view_inst, request)

        elif report_id is not None:
            rv_report = self.session.query(self.reports_cls).filter_by(
                id=report_id).first()

            if rv_report.form is not None:
                try:
                    form_data = json.loads(rv_report.form)
                    kwargs = view_inst.run(form=form_data)
                except (ValueError) as e:
                    # get_logger().info('Unable to parse json form' + str(rv_report.form)+ e.args[0])
                    kwargs = view_inst.run(form=view_inst.form())

            else:
                kwargs = view_inst.run(form=view_inst.form())

        kwargs['form'] = view_inst.form()

        return kwargs


    def get_user_views(self, user=None):
        """ Gets of the current user views as a directed graph

        :param username: The current users username
        :type username: str
        :return: A directed graph representation of the user views
        :rtype : nx.Digraph

        """
        if user is None:
            user=current_user
        rv = self.session.query(self.reports_cls).join(self.user_cls) \
            .filter(self.user_cls.id == user.id).all()  # .filter_by(user_id=user_id)
        G = nx.DiGraph()
        for view in rv:
            if view.analysis_id is None:
                G.add_node(view.id, name=view.name,
                           report_id=view.id, analysis_id=None)
            else:
                analysis_rv = self.session.query(
                    self.analysis_cls).filter_by(id=view.analysis_id).first()
                if analysis_rv is not None:
                    G.add_node(view.id, name=view.name,
                               analysis_id=analysis_rv.id, report_id=view.id)
                else:
                    pass
            if view.parent_id is not None:
                print(view.parent_id, view.id)
                G.add_edge(view.parent_id, view.id)
        return G


def make_rel(html_path):
    """ makes the path to a file relative

    :param html_path: A string representing the path to a file
    :return: the relative path of the file
    """
    matching = re.search('^/(.*)', html_path)
    return matching.groups(1)[0]


def clone(el):
    if isinstance(el, NavigableString):
        return type(el)(el)

    copy = Tag(None, el.builder, el.name, el.namespace, el.nsprefix)
    # work around bug where there is no builder set
    # https://bugs.launchpad.net/beautifulsoup/+bug/1307471
    copy.attrs = dict(el.attrs)
    for attr in ('can_be_empty_element', 'hidden'):
        setattr(copy, attr, getattr(el, attr))
    for child in el.contents:
        copy.append(clone(child))
    return copy


def add_tfoot_table(htmltable):
    '''Takes a header tag and copies them into a footer tag'''
    headertag = htmltable.find('thead')
    foottag = clone(headertag)
    foottag.name = 'tfoot'
    htmltable.find('table').append(foottag)

    return htmltable
