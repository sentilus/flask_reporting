from flask import (Blueprint, render_template, current_app, request, g,
                   flash, url_for, redirect, abort)
from flask_security import login_required,  current_user

from . import models
from . import utils
from .forms import ReportNameForm
from .base import Reporting

reporting = Blueprint('reporting', __name__, url_prefix='reporting', template_folder="templates",
                                           static_url_path="/static/reporting",
                                           static_folder="static")


def public_base(session, user_cls, reports_cls, analysis_cls):
    G = utils.get_user_views(session, current_user, user_cls,
                             reports_cls, analysis_cls)
    roots = [n for n, d in G.in_degree().items() if d == 0]
    return render_template(
        'reporting/base_report.html', G=G, roots=roots,
        branding=Reporting.branding, base_template=Reporting.base_template,
        appbuilder=Reporting.appbuilder)


def get_analysis_from_module_list(analysis, classname):
    """ Gets the current views analysis from a list of modules with analysis
    :param analysis: A list of module containing analysis
    :rtype : sentiweb.common_app.utils.IBaseAnalysis
    """
    for viewclass in analysis:
        if viewclass.__name__ == classname:
            return viewclass
    return None


def save_analysis_base(session, analysis_id, report_id, reports_cls, analysis_cls):
    rv = session.query(analysis_cls).filter_by(
        id=analysis_id).first()
    viewclass = get_analysis_from_module_list(Reporting.all_analyses,
                                              rv.classname)
    return utils.save_current_config(report_id, viewclass(), request,
                                     session, reports_cls)


def model_view_base(session, analysis_id, report_id,
                    analysis_cls, reports_cls, user_cls):
    rv = session.query(analysis_cls).filter_by(
        id=analysis_id).first()
    viewclass = get_analysis_from_module_list(Reporting.all_analyses,
                                              rv.classname)
    if viewclass is not None:
        view_inst = viewclass()
        G = utils.get_user_views(session, current_user, user_cls,
                                 reports_cls, analysis_cls)

        if hasattr(view_inst, 'form'):
            kwargs = utils.get_kwargs(request, view_inst, report_id,
                                      session, reports_cls)
        else:
            kwargs = view_inst.run()
            kwargs['form'] = None
        kwargs['G'] = G
        kwargs['roots'] = [n for n, d in G.in_degree().items() if d == 0]
        kwargs['ReportNameForm'] = ReportNameForm()
        kwargs['template'] = view_inst.template()

        kwargs['branding'] = Reporting.branding
        kwargs['base_template'] = Reporting.base_template
        if Reporting.appbuilder is not None:
            kwargs['appbuilder'] = Reporting.appbuilder

        return render_template('reporting/base_report.html', **kwargs)
    else:
        return abort(500)


# @reporting.route('/public')
# @login_required
# def public():
#     """Public interface shows branding and report tree"""
#     return public_base()
#
# @reporting.route('/<int:analysis_id>/<int:report_id>/save', methods=['POST'])
# @login_required
# def savemodel(analysis_id, report_id=None):
#     """ Allows saving of a reports"""
#     return save_analysis_base(analysis_id, report_id)
#
#
#
# @reporting.route('/<int:analysis_id>/<int:report_id>', methods=['GET', 'POST'])
# @login_required
# def modelview(analysis_id, report_id=None):
#     """ The main routing for flask reporting"""
#
#     return model_view_base(analysis_id, report_id)
#
# @reporting.app_errorhandler(500)
# def internal_server_error(e):
#     return render_template('500.html'), 500



