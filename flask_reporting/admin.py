from flask.ext.admin.contrib.sqla import ModelView
from flask_login import current_user
from .models import SessionAnalysis, SessionReports
from ..extensions import admin, db

class ViewKeys(ModelView):
    # column_display_pk = True  # optional, but I like to see the IDs in the list
    # column_hide_backrefs = False
    # form_hide_backrefs = False
    # # form_columns  = attribute_names(datamodels.session_reports)
    # # #3print form_columns
    # # column_list=attribute_names(datamodels.session_reports)

    def is_accessible(self):
        return current_user.is_authenticated()

admin_views=[ViewKeys(SessionAnalysis, db.session,category="Saved Reports"), ViewKeys(SessionReports, db.session,category="Saved Reports")]
