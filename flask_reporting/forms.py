from flask_wtf import Form
import wtforms

class ReportNameForm(Form):
    report_name = wtforms.fields.TextField('Report Name')
