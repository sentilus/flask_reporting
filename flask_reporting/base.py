import abc


class Reporting(object):
    """ Base class of the reporting extension

        Can be initialised on the app in one of two ways:

        >>> frontend = Reporting(app, db, user)

        or

        >>> frontend = Reporting()
        >>> frontend.init_app(app, db, user)

    """
    appbuilder = None
    all_analyses = None
    base_template = None
    branding = None
    # db = None
    # User = None

    def __init__(self, app=None, all_analyses=None, appbuilder=None,
                 base_template='reporting_imports.html',
                 branding=None, **kwargs):
        """ Constructor

            :type app: flask.Flask
            :param app: Application object
            :type db: flask.ext.sqlalchemy.SQLAlchemy
            :param db: Flask-SQLAlchemy object linked to the app
            :param db: Flask-SQLAlchemy ORM user object contains unique name field

        """
        # User = user
        for k, v in kwargs.items():
            setattr(self, k, v)

        if app is not None:
            self.init_app(app, all_analyses=all_analyses,
                          base_template=base_template, branding=branding,
                          appbuilder=appbuilder, **kwargs)

    def init_app(self, app, all_analyses=None,
                 base_template='reporting_imports.html', appbuilder=None,
                 branding=None, **kwargs):


        # assert db is not None, "db required to initialise extension"
        # assert user is not None, "user class required to initialise extension"

        # self.db = db
        # self.User = user
        # self.base_template = base_template
        # self.branding = branding

        Reporting.all_analyses = all_analyses
        if Reporting.all_analyses is None:
            Reporting.all_analyses = []

        Reporting.base_template = base_template
        Reporting.branding = branding
        # Reporting.db = db
        # Reporting.User = user
        Reporting.appbuilder = appbuilder

        from .views import reporting

        app.register_blueprint(reporting, url_prefix='/reporting',
                               template_folder="templates",
                               static_url_path="/static/reporting",
                               static_folder="static")

        return app

    def add_analysis(self, analysis):
        Reporting.all_analyses.append(analysis)

    def populate_base_config(self):
        from . import collector

        collector.populate_analysis_reports(Reporting.all_analyses)


class IBaseAnalysis(object):
    """Contains the base class of for an analysis"""

    __metaclass__ = abc.ABCMeta

    def isinteractive(self):
        return False

    def set_form_choices(self, form):

        return form

    def setup_form(self, form):
        if type(form) == dict:
            form = self.form().__class__(**form)
        if form is None:
            form = self.form().__class__()

        form = self.set_form_choices(form)

        return form

    @abc.abstractmethod
    def run(self):
        """

        :rtype : dict
        """
        return dict()

    @abc.abstractproperty
    def name(self):
        """

        :rtype : str
        """
        pass

    @abc.abstractproperty
    def form(self):
        """

        :rtype : str
        """
        pass
