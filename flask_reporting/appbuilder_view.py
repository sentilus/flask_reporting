from flask_appbuilder import BaseView, expose, has_access
from flask_reporting import Reporting


from .views import model_view_base, public_base, save_analysis_base
from flask import (Blueprint, render_template, current_app, request, g,
                   flash, url_for, redirect, session, abort)
from flask_security import login_required,  current_user

from . import utils
from .utils import ReportingUtility
from .forms import ReportNameForm
from .base import Reporting
from .views import get_analysis_from_module_list
import json

#TODO add security decorators
class ReportView(BaseView, ReportingUtility):
    default_view = 'public'

    def __init__(self, session, user_cls, reports_cls, analysis_cls,tables_cls,
                 *args, **kwargs):

        # super(ReportView, self).__init__(*args, **kwargs)
        BaseView.__init__(self, *args, **kwargs)
        ReportingUtility.__init__(self, session, user_cls,
                                  reports_cls, analysis_cls,tables_cls)

    def render(self,template, **kwargs):
        return self.render_template(template, **kwargs)

    @expose('/')
    @has_access
    def public(self):
        """Public interface shows branding and report tree"""
        # return public_base()

        G = self.get_user_views(current_user)
        roots = [n for n, d in G.in_degree().items() if d == 0]
        return self.render(
            'reporting/app_builder_base_report.html', G=G, roots=roots,
            branding=Reporting.branding, base_template=Reporting.base_template)

    @expose('/<int:analysis_id>/<int:report_id>/save', methods=['POST'])
    @has_access
    def savemodel(self, analysis_id, report_id=None):
        """ Allows saving of a reports"""
        rv = self.session.query(self.analysis_cls).filter_by(
            id=analysis_id).first()
        viewclass = get_analysis_from_module_list(Reporting.all_analyses,
                                                  rv.classname)
        if viewclass is not None:
            view_inst = viewclass()
        return self.save_current_config(report_id, view_inst, request)

    @expose('/<int:analysis_id>/<int:report_id>', methods=['GET', 'POST'])
    @has_access
    def modelview(self, analysis_id, report_id=None):
        """ The main routing for flask reporting"""
        rv = self.session.query(self.analysis_cls).filter_by(
            id=analysis_id).first()
        viewclass = get_analysis_from_module_list(Reporting.all_analyses,
                                                  rv.classname)
        if viewclass is not None:
            view_inst = viewclass()
            G = self.get_user_views(current_user)

            if hasattr(view_inst, 'form'):
                kwargs = self.get_kwargs(request, view_inst, report_id)

            else:
                kwargs = view_inst.run()
                kwargs['form'] = None
            kwargs['G'] = G
            kwargs['roots'] = [n for n, d in G.in_degree().items() if d == 0]
            kwargs['ReportNameForm'] = ReportNameForm()
            kwargs['reporting_template'] = view_inst.template()

            kwargs['base_template'] = Reporting.base_template

            return self.render(kwargs['reporting_template'], **kwargs)
        else:
            return abort(500)

    @expose('/<int:analysis_id>/<int:report_id>/loadState', methods=['POST'])
    @has_access
    def loadtable(self, analysis_id, report_id=None):
        """ Allows saving of datatables state"""
        data=self.session.\
            query(self.table_cls).filter(self.table_cls.report_id==report_id
        ).first()
        if data is None:
            return ''
        else:
            return data.json_config

    @expose('/<int:analysis_id>/<int:report_id>/saveState', methods=['POST'])
    @has_access
    def savetable(self, analysis_id, report_id=None):
        """ Allows saving of a reports datatables state"""

        newObj=self.session.query(self.table_cls).\
            filter(self.table_cls.report_id==report_id).first()
        if newObj is None:
            newObj = self.table_cls()
        newObj.report_id = report_id
        newObj.json_config = json.dumps(request.json)
        newObj.table_id = 1
        self.session.merge(newObj)
        self.session.commit()
        return 'TableStateSaved'