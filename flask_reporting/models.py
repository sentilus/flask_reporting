from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.orm import relationship, backref
from sqlalchemy.ext.declarative import declared_attr
from sqlalchemy.ext.hybrid import hybrid_property
from .base import Reporting
# from flask_appbuilder import Model
# from Reporting.db import Model
# Model = Reporting.db.Model

STRING_LEN = 500


class ReportsMixin(object):

    """ ORM for the session_reports table. Requires a User class """

    __tablename__ = 'session_reports'

    id = Column(Integer, primary_key=True, autoincrement=True)
    form = Column(String)
    name = Column(String)

    analysis_model = None
    user_model = None
    table_model = None

    @hybrid_property
    def self_name(cls):
        return cls.__name__

    @declared_attr
    def analysis(cls):
        return relationship(cls.analysis_model)

    # backref=backref("parent", remote_side='SessionReports.id')

    @declared_attr
    def children(cls):
        return relationship(cls.self_name,  # ,#,

                                # cascade deletions
                                # cascade="all",

                                # many to one + adjacency list - remote_side
                                # is required to reference the 'remote'
                                # column in the join condition.
                                backref=backref("parent",
                                                remote_side=cls.self_name + '.id'),

                                # children will be represented as a dictionary
                                # on the "name" attribute.
                                #collection_class=attribute_mapped_collection('name'),
        )

    @declared_attr
    def user(cls):
        return relationship('User')

    @declared_attr
    def table(cls):
        return relationship('SessionSavedTables', cascade="all")

    @declared_attr
    def parent_id(cls):
        return Column(Integer, ForeignKey(cls.__tablename__ + '.id'))

    @declared_attr
    def analysis_id(cls):
        return Column(Integer, ForeignKey(cls.analysis_model.id))

    @declared_attr
    def user_id(cls):
        return Column(Integer, ForeignKey(cls.user_model.id))


    show_form_names = ['parent', 'children', 'name', 'form', 'analysis', 'user']


    def __repr__(self):
        if self.analysis is None:
            name_analysis = 'Group'
        else:
            name_analysis = self.analysis

        if self.parent is None:

            name_parent = 'Base'
        else:
            name_parent = self.parent
        return "%s -> %s - %s" % (
            name_parent, name_analysis, self.name)


class SavedTablesMixin(object):

    """Contains Datatables json data"""

    __tablename__ = 'session_saved_table'
    reports_model = None

    id = Column(Integer, primary_key=True, autoincrement=True)
    #report_id = Column(Integer, ForeignKey('session_reports.id'))

    @declared_attr
    def report_id(cls):
        return Column(Integer, ForeignKey(cls.reports_model.id))

    @declared_attr
    def report(cls):
        return relationship('SessionReports')

    json_config = Column(String)
    table_id = Column(Integer)

    def __repr__(self):
        return "Table: %s %s " % (str(self.table_id), self.report)


class AnalysisMixin(object):

    """ORM for the session_analysis table. """

    __tablename__ = 'session_analysis'

    id = Column(Integer, primary_key=True, autoincrement=True, nullable=False)
    classname = Column(String(20), unique=True)
    name = Column(String)

    def __repr__(self):
        return " %s " % (self.name)


